package com.movie.moviems.repositories;

import com.movie.moviems.models.Movie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface MovieRepository extends JpaRepository<Movie, Long> {
    // Define custom queries if needed

    @Query(nativeQuery = true, value = "SELECT * FROM movies_data where id in :ids and language = :language")
    Optional<List<Movie>> findMoviesByLanguage(@Param("ids") List<Long> ids, @Param("language") String language);
    @Query(nativeQuery = true, value = "SELECT * FROM movies_data where id in :ids and genre = :genre")
    Optional<List<Movie>> findMoviesByGenre(@Param("ids") List<Long> ids, @Param("genre") String genre);

    @Query(nativeQuery = true, value = "SELECT * FROM movies_data where id in :ids and language = :language and genre = :genre")
    Optional<List<Movie>> findMoviesByLanguageAndGenre(@Param("ids") List<Long> ids, @Param("language") String language, @Param("genre") String genre);

}
