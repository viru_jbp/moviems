package com.movie.moviems.repositories;

import com.movie.moviems.models.RunningMovie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface RunningMovieRepository extends JpaRepository<RunningMovie, Long> {
    @Query(nativeQuery = true, value = "SELECT * FROM running_movies_data where city=:city")
    Optional<List<RunningMovie>> findMoviesByCity(@Param("city") String city);
}
