package com.movie.moviems.controllers;

import com.movie.moviems.models.Movie;
import com.movie.moviems.models.MovieDetails;
import com.movie.moviems.models.RunningMovie;
import com.movie.moviems.services.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/movies")
public class MovieController {
    private final MovieService movieService;

    @Autowired
    public MovieController(MovieService movieService) {
        this.movieService = movieService;
    }

    @GetMapping
    public List<Movie> getAllMovies() {
        return movieService.getAllMovies();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Movie> getMovieById(@PathVariable Long id) {
        Movie movie = movieService.getMovieById(id);
        if (movie != null) {
            return ResponseEntity.ok(movie);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("/running")
    public List<MovieDetails> browseMovies(
            @RequestParam(name = "city", required = false) String city,
            @RequestParam(name = "language", required = false) String language,
            @RequestParam(name = "genre", required = false) String genre
    ) {
        List<MovieDetails> movies;

        if (city != null && language != null && genre != null) {
            // Fetch movies by all criteria (city, language, genre)
            movies = movieService.getMoviesByCityLanguageAndGenre(city, language, genre);
        } else if (city != null && language != null) {
            // Fetch movies by city and language
            movies = movieService.getMoviesByCityAndLanguage(city, language);
        } else if (city != null && genre != null) {
            // Fetch movies by city and genre
            movies = movieService.getMoviesByCityAndGenre(city, genre);
        } else if (language != null && genre != null) {
            // Fetch movies by language and genre
            movies = movieService.getMoviesByLanguageAndGenre(language, genre);
        } else if (city != null) {
            // Fetch movies by city
            movies = movieService.getMoviesByCity(city);
        } else if (language != null) {
            // Fetch movies by language
            movies = movieService.getMoviesByLanguage(language);
        } else if (genre != null) {
            // Fetch movies by genre
            movies = movieService.getMoviesByGenre(genre);
        } else {
            // Fetch all movies (no filter applied)
            movies = movieService.getAllRunningMovies();
        }

        return movies;
    }

    @PostMapping("/addMovies")
    public ResponseEntity<Movie> addMovies(@RequestBody Movie movie) {
        Movie addMovie = movieService.addMovieInDB(movie);
        return ResponseEntity.status(HttpStatus.CREATED).body(addMovie);
    }

    @PostMapping("/addRunningMovie")
    public ResponseEntity<RunningMovie> addRunningMovies(@RequestBody RunningMovie runningMovie) {
        RunningMovie addRunningMovie = movieService.addRunningMovieInDB(runningMovie);
        return ResponseEntity.status(HttpStatus.CREATED).body(addRunningMovie);
    }

}
