package com.movie.moviems.services;

import com.movie.moviems.exception.ResourceNotFoundException;
import com.movie.moviems.models.Movie;
import com.movie.moviems.models.MovieDetails;
import com.movie.moviems.models.RunningMovie;
import com.movie.moviems.repositories.MovieRepository;
import com.movie.moviems.repositories.RunningMovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class MovieService {
    private final MovieRepository movieRepository;
    @Autowired
    private RunningMovieRepository runningMovieRepository;

    @Autowired
    public MovieService(MovieRepository movieRepository) {
        this.movieRepository = movieRepository;
    }

    public List<Movie> getAllMovies(){
        try {
            return movieRepository.findAll();
        } catch (Exception e) {
         throw e;
        }
    }
    public List<MovieDetails> getAllRunningMovies() {
        List<RunningMovie> runningMovies = runningMovieRepository.findAll();

        if (runningMovies.isEmpty()) {
            throw new ResourceNotFoundException("No Running Movies");
        }

        List<Long> movieIds = runningMovies.stream()
                .map(RunningMovie::getMovieId)
                .collect(Collectors.toList());

        List<Movie> movieList = movieRepository.findAllById(movieIds);

        Map<Long, Movie> movieMap = movieList.stream()
                .collect(Collectors.toMap(Movie::getId, movie -> movie));

        return runningMovies.stream()
                .map(runningMovie -> {
                    Movie movieData = movieMap.get(runningMovie.getMovieId());

                    if (movieData != null) {
                        return getMovieDetails(runningMovie, movieData);
                    } else {
                        throw new ResourceNotFoundException("No Running Movies found");
                    }
                })
                .collect(Collectors.toList());
    }

    public Movie getMovieById(Long id) {
        return movieRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("No Movie found with id " + id));
    }

    public List<MovieDetails> getMoviesByCity(String city) {
        List<RunningMovie> runningMovies = runningMovieRepository.findMoviesByCity(city)
                .orElseThrow(() -> new ResourceNotFoundException("No Movie found in city " + city));

        List<Long> movieIds = runningMovies.stream()
                .map(RunningMovie::getMovieId)
                .collect(Collectors.toList());

        Map<Long, Movie> movieMap = movieRepository.findAllById(movieIds)
                .stream()
                .collect(Collectors.toMap(Movie::getId, movie -> movie));

        return runningMovies.stream()
                .map(runningMovie -> {
                    Movie movieData = movieMap.get(runningMovie.getMovieId());
                    if (movieData != null) {
                        return getMovieDetails(runningMovie, movieData);
                    } else {
                        throw new ResourceNotFoundException("No Movie found in city " + city);
                    }
                })
                .collect(Collectors.toList());
    }

    public List<MovieDetails> getMoviesByLanguage(String language) {
        List<RunningMovie> runningMovies = runningMovieRepository.findAll();

        if (runningMovies.isEmpty()) {
            throw new ResourceNotFoundException("No Running Movies found with language " + language);
        }

        List<Long> movieIds = runningMovies.stream()
                .map(RunningMovie::getMovieId)
                .collect(Collectors.toList());

        Map<Long, Movie> movieMap = movieRepository.findMoviesByLanguage(movieIds, language)
                .orElseThrow(() -> new ResourceNotFoundException("No Running Movies found with language " + language))
                .stream()
                .collect(Collectors.toMap(Movie::getId, movie -> movie));

        return runningMovies.stream()
                .map(runningMovie -> {
                    Movie movieData = movieMap.get(runningMovie.getMovieId());

                    if (movieData != null) {
                        return getMovieDetails(runningMovie, movieData);
                    } else {
                        throw new ResourceNotFoundException("No Running Movies found with language " + language);
                    }
                })
                .collect(Collectors.toList());
    }

    public List<MovieDetails> getMoviesByGenre(String genre) {
        List<RunningMovie> runningMovies = runningMovieRepository.findAll();

        if (runningMovies.isEmpty()) {
            throw new ResourceNotFoundException("No Running Movies found with genre " + genre);
        }

        List<Long> movieIds = runningMovies.stream()
                .map(RunningMovie::getMovieId)
                .collect(Collectors.toList());

        Map<Long, Movie> movieMap = movieRepository.findMoviesByGenre(movieIds, genre)
                .orElseThrow(() -> new ResourceNotFoundException("No Running Movies found with genre " + genre))
                .stream()
                .collect(Collectors.toMap(Movie::getId, movie -> movie));

        return runningMovies.stream()
                .map(runningMovie -> {
                    Movie movieData = movieMap.get(runningMovie.getMovieId());
                    if (movieData != null) {
                        return getMovieDetails(runningMovie, movieData);
                    } else {
                        throw new ResourceNotFoundException("No Running Movies found with genre " + genre);
                    }
                })
                .collect(Collectors.toList());
    }

    public List<MovieDetails> getMoviesByLanguageAndGenre(String language, String genre) {
        List<RunningMovie> runningMovies = runningMovieRepository.findAll();

        if (runningMovies.isEmpty()) {
            throw new ResourceNotFoundException("No Running Movies found with language " + language + " and genre " + genre);
        }

        List<Long> movieIds = runningMovies.stream()
                .map(RunningMovie::getMovieId)
                .collect(Collectors.toList());

        Map<Long, Movie> movieMap = movieRepository.findMoviesByLanguageAndGenre(movieIds, language, genre)
                .orElseThrow(() -> new ResourceNotFoundException("No Running Movies found with language " + language + " and genre " + genre))
                .stream()
                .collect(Collectors.toMap(Movie::getId, movie -> movie));

        return runningMovies.stream()
                .map(runningMovie -> {
                    Movie movieData = movieMap.get(runningMovie.getMovieId());
                    if (movieData != null) {
                        return getMovieDetails(runningMovie, movieData);
                    } else {
                        throw new ResourceNotFoundException("No Running Movies found with language " + language + " and genre " + genre);
                    }
                })
                .collect(Collectors.toList());
    }

    public List<MovieDetails> getMoviesByCityAndGenre(String city, String genre) {
        List<RunningMovie> runningMovies = runningMovieRepository.findMoviesByCity(city)
                .orElseThrow(() -> new ResourceNotFoundException("No Running Movies found with city " + city + " and genre " + genre));

        List<Long> movieIds = runningMovies.stream()
                .map(RunningMovie::getMovieId)
                .collect(Collectors.toList());

        Map<Long, Movie> movieMap = movieRepository.findMoviesByGenre(movieIds, genre)
                .orElseThrow(() -> new ResourceNotFoundException("No Running Movies found with city " + city + " and genre " + genre))
                .stream()
                .collect(Collectors.toMap(Movie::getId, movie -> movie));

        return runningMovies.stream()
                .map(runningMovie -> {
                    Movie movieData = movieMap.get(runningMovie.getMovieId());
                    if (movieData != null) {
                        return getMovieDetails(runningMovie, movieData);
                    } else {
                        throw new ResourceNotFoundException("No Running Movies found with city " + city + " and genre " + genre);
                    }
                })
                .collect(Collectors.toList());
    }

    public List<MovieDetails> getMoviesByCityAndLanguage(String city, String language) {
        List<RunningMovie> runningMovies = runningMovieRepository.findMoviesByCity(city)
                .orElseThrow(() -> new ResourceNotFoundException("No Running Movies found with city " + city + " and language " + language));

        List<Long> movieIds = runningMovies.stream()
                .map(RunningMovie::getMovieId)
                .collect(Collectors.toList());

        Map<Long, Movie> movieMap = movieRepository.findMoviesByLanguage(movieIds, language)
                .orElseThrow(() -> new ResourceNotFoundException("No Running Movies found with city " + city + " and language " + language))
                .stream()
                .collect(Collectors.toMap(Movie::getId, movie -> movie));

        return runningMovies.stream()
                .map(runningMovie -> {
                    Movie movieData = movieMap.get(runningMovie.getMovieId());
                    if (movieData != null) {
                        return getMovieDetails(runningMovie, movieData);
                    } else {
                        throw new ResourceNotFoundException("No Running Movies found with city " + city + " and language " + language);
                    }
                })
                .collect(Collectors.toList());
    }

    public List<MovieDetails> getMoviesByCityLanguageAndGenre(String city, String language, String genre) {
        List<RunningMovie> runningMovies = runningMovieRepository.findMoviesByCity(city)
                .orElseThrow(() -> new ResourceNotFoundException("No Running Movies found with city " + city + ", genre " + genre + ", and language " + language));

        List<Long> movieIds = runningMovies.stream()
                .map(RunningMovie::getMovieId)
                .collect(Collectors.toList());

        Map<Long, Movie> movieMap = movieRepository.findMoviesByLanguageAndGenre(movieIds, language, genre)
                .orElseThrow(() -> new ResourceNotFoundException("No Running Movies found with city " + city + ", genre " + genre + ", and language " + language))
                .stream()
                .collect(Collectors.toMap(Movie::getId, movie -> movie));

        return runningMovies.stream()
                .map(runningMovie -> {
                    Movie movieData = movieMap.get(runningMovie.getMovieId());
                    if (movieData != null) {
                        return getMovieDetails(runningMovie, movieData);
                    } else {
                        throw new ResourceNotFoundException("No Running Movies found with city " + city + ", genre " + genre + ", and language " + language);
                    }
                })
                .collect(Collectors.toList());
    }


    public Movie addMovieInDB(Movie movie){
        try {
            return movieRepository.save(movie);
        } catch (Exception e){
            throw e;
        }
    }
    public RunningMovie addRunningMovieInDB(RunningMovie runningMovie) {
        try {
            return runningMovieRepository.save(runningMovie);
        } catch (Exception e) {
            throw e;
        }
    }

    private MovieDetails getMovieDetails(RunningMovie runningMovie, Movie movieData){
        try {
            MovieDetails movieDetails = new MovieDetails();
            movieDetails.setCity(runningMovie.getCity());
            movieDetails.setTheatreId(runningMovie.getTheatreId());
            movieDetails.setTitle(movieData.getTitle());
            movieDetails.setDescription(movieData.getDescription());
            movieDetails.setGenre(movieData.getGenre());
            movieDetails.setLanguage(movieData.getLanguage());
            movieDetails.setReleaseDate(movieData.getReleaseDate());
            return movieDetails;
        } catch (Exception e) {
            throw e;
        }
    }

}
