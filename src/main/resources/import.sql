insert into movies_data (title, language, genre, description, release_date) values ('Rock2', 'English', 'Action', 'This is action movie', '2023-09-05 00:00:00');
insert into movies_data (title, language, genre, description, release_date) values ('Techno', 'English', 'Science', 'This is science movie', '2021-09-04 00:00:00');


insert into running_movies_data (city, movie_id, theatre_id) values ('Bengaluru', '2', '2');
insert into running_movies_data (city, movie_id, theatre_id) values ('Mumbai', '1', '1');
